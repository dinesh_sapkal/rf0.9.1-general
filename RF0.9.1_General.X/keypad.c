#include "basic_requirements.h"

//#define motor_delay_ON()	__delay_ms(1000)
//#define motor_delay_OFF()	__delay_ms(50)

unsigned char gucRx_Product_Code[5]={0,0,0,0,0};


const unsigned char Procudt_Code[72][6]={
	"001a", //0

	"0000", //1 A 1
	"0001", //2   2
	"0002", //3   3
	"0003", //4   4
	"0004", //5   5
	"0005", //6   6
	"0006", //7   7
	"0007", //8   8
	"0008", //9   9
	"0009",// 10  a9

	"0100", //1 B 11
	"0101", //2   12
	"0102", //3   13
	"0103", //4   14
	"0104", //5   15
	"0105", //6   16
	"0106", //7   17
	"0107", //8   18
	"0108", //9   19
	"0109", //10  20

	"0200", //1 C 21
	"0201", //2   22
	"0202", //3   23
	"0203", //4   24
	"0204", //5   25
	"0205", //6   26
	"0206", //7   27
	"0207", //8   28
	"0208", //9   29
	"0209", //10  30

	"0300", //1 D 31
	"0301", //2   32
	"0302", //3   33
	"0303", //4   34
	"0304", //5   35
	"0305", //6   36
	"0306", //7   37
	"0307", //8   38
	"0308", //9	  39
	"0309", //10  40

	"0400", //1 E 41
	"0401", //2   42
	"0402", //3   43
	"0403", //4   44
	"0404", //5   45
	"0405", //6   46
	"0406", //7   47
	"0407", //8   48
	"0408", //9   49
	"0409", //10  50

	"0500", //1 F 51
	"0501", //2   52
	"0502", //3   53
	"0503", //4   54
	"0504", //5   55
	"0505", //6   56
	"0506", //7   57
	"0507", //8   58
	"0508", //9   59
	"0509", //10  60

	"0600", //1 F 61
	"0601", //2   62
	"0602", //3   63
	"0603", //4   64
	"0604", //5   65
	"0605", //6   66
	"0606", //7   67
	"0607", //8   68
	"0608", //9   69
	"0609" //10  70
};

const unsigned char Payment_Code[36][8]={
"7A7058C",
"9A4098D",
"BA70A89",
"EAD0C81",
"0A4098D",
"DA9038F",
"EA60182",
"3A60888",
"7AE068D",
"7AF0B8F",
"5A70B8A",
"0AE068C",
"8A6088F",
"3AA0E82",
"1AA0988",
"BA7038C",
"FA70B8C",
"8A50E89",
"6AF0E8D",
"DAA0783",
"1A5078B",
"EA10589",
"AA9058D",
"5AD0E85",
"EAD0C8F",
"8A90F80",
"7A30D8A",
"0A00D88",
"5AD0783",
"EAF0B81",
"7AB0D8D",
"AA90986",
"3A80A80",
"DA00C87",
"FA70685",
"7AD0383"
};



unsigned int compare_strings(const unsigned char a[], unsigned char b[])
{
	int c = 0;
	
	while (a[c] == b[c])
	{
		if (a[c] == '\0' || b[c] == '\0')
		{
            break;
        }
		c++;
	}
	
	if (a[c] == '\0' && b[c] == '\0')
    {
		return 0;
    }
	else
    {
		return 1;
    }
}


void  motor_delay_ON()
{
//    for(long ic=0;ic<=150000;ic++);
    #ifdef SEGA_NUMERIC_MACHINE
        for(long ic=0;ic<=150000;ic++);
    #endif

    #ifdef SEGA_ALPHANUMERIC_MACHINE
       for(long ic=0;ic<=150000;ic++);
    #endif

    #ifdef VENDIMAN_XY_MACHINE 
        for(long ic=0;ic<=100000;ic++);
    #endif

    #ifdef TCN_COMBO_WHITE_MACHINE
       for(long ic=0;ic<=75000;ic++);
    #endif

    #ifdef VEND_STOP_BLACK_MACHINE
        for(long ic=0;ic<=150000;ic++);
    #endif
}
void motor_delay_OFF()
{
//    for(long ic=0;ic<=150000;ic++);
    #ifdef SEGA_NUMERIC_MACHINE
        for(long ic=0;ic<=150000;ic++);
    #endif

    #ifdef SEGA_ALPHANUMERIC_MACHINE
       for(long ic=0;ic<=150000;ic++);
    #endif

    #ifdef VENDIMAN_XY_MACHINE 
        for(long ic=0;ic<=100000;ic++);
    #endif

    #ifdef TCN_COMBO_WHITE_MACHINE
       for(long ic=0;ic<=75000;ic++);
    #endif

    #ifdef VEND_STOP_BLACK_MACHINE
        for(long ic=0;ic<=150000;ic++);
    #endif
}


void PressMachineKey(unsigned char gucMCMotorNumber)
{
#ifdef SEGA_NUMERIC_MACHINE	
	switch(gucMCMotorNumber)
	{
		case 11: //A1 ,100
		
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
		
				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');

				break;
		
		case 12: //A1 ,101
		
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				break;

		case 13: //A2
		
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

		
				break;

		case 14: //A3
		
            	KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

		
				break;
		
		case 15: //A4
		
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

		
				break;

		case 16: //A5
		
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				break;

		case 17: //A6
                KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

		
				break;

		case 18: //A7
                KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_7_SeagaN =1;
				motor_delay_ON();
				KEY_7_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('7');

		
				break;

		case 19: //A8
                KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_8_SeagaN =1;
				motor_delay_ON();
				KEY_8_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('8');

		
				break;

		case 20: //A9
                KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_9_SeagaN =1;
				motor_delay_ON();
				KEY_9_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('9');

				break;

		case 21: //110
            	KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');


				break;

		case 22: //B1 201
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				break;

		case 23: //B2
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');
				break;

		case 24: //B3
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');
				break;

		case 25://B4
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');
				break;
		
		case 26://B5
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');
				break;

		case 27: //B6
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');
				break;

		case 28: //B7
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_7_SeagaN =1;
				motor_delay_ON();
				KEY_7_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('7');
				break;

		case 29: //B8
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_8_SeagaN =1;
				motor_delay_ON();
				KEY_8_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('8');
				break;
		
		case 30: //B9
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
		
				KEY_9_SeagaN =1;
				motor_delay_ON();
				KEY_9_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('9');
				break;

		case 31: //B10 210
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
		
				break;
		

		case 32: //C1
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');

				break;

		case 33: //C2
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');
				break;
		
		case 34: //C3
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');
				break;

		case 35: //C4
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');
				break;

		case 36: //C5
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');
				break;
		

		case 37: //C6
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');
		
				break;

		case 38: //C7
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_7_SeagaN =1;
				motor_delay_ON();
				KEY_7_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('7');
				break;

		case 39: //C8
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_8_SeagaN =1;
				motor_delay_ON();
				KEY_8_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('8');
				break;
		
		case 40: //C9
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_9_SeagaN =1;
				motor_delay_ON();
				KEY_9_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('9');
				break;

		case 41: //C10 310
//            BLE_PutString("\r\n41");
                KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				break;

		case 42: //D1

				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');
				break;

		case 43: //D2

				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');
				break;

		case 44://D3

				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');
				break;

		case 45: //D4
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');
				break;

		case 46: //D5
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');
				break;
		
		case 47: //D6
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');
				break;

		case 48: //D7
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_7_SeagaN =1;
				motor_delay_ON();
				KEY_7_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('7');
				break;

		case 49: //D8
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_8_SeagaN =1;
				motor_delay_ON();
				KEY_8_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('8');
				break;

		case 50: //D9
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_9_SeagaN =1;
				motor_delay_ON();
				KEY_9_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('9');
				break;
		
		case 51: //D10 410
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				break;
		
		case 52: //E1
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');
				break;
		
		case 53: //E2
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');
				break;

		case 54: //E3
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');
				break;

		case 55: //E4
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');
				break;

		case 56: //E5
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');
				break;


		case 57: //E6
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');
				break;
		
		case 58: //E7
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_7_SeagaN =1;
				motor_delay_ON();
				KEY_7_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('7');
				break;

		case 59: //E8
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_8_SeagaN =1;
				motor_delay_ON();
				KEY_8_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('8');
				break;

		case 60: //E9
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_9_SeagaN =1;
				motor_delay_ON();
				KEY_9_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('9');
				break;

		case 61: //E10 510
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				break;

		case 62: //F1
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_1_SeagaN = 1;
				motor_delay_ON();
				KEY_1_SeagaN = 0;
				motor_delay_OFF();
				//Debug_PutChar('1');
				break;
		
		case 63: //F2
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_2_SeagaN =1;
				motor_delay_ON();
				KEY_2_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('2');
				break;

		case 64: //F3
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_3_SeagaN =1;
				motor_delay_ON();
				KEY_3_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('3');
				break;

		case 65: //F4
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_4_SeagaN =1;
				motor_delay_ON();
				KEY_4_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('4');
				break;

		case 66: //F5
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_5_SeagaN =1;
				motor_delay_ON();
				KEY_5_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('5');
				break;


		case 67: //F6
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');
				break;
		
		case 68: //F7
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_7_SeagaN =1;
				motor_delay_ON();
				KEY_7_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('7');
				break;

		case 69: //F8
		KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_8_SeagaN =1;
				motor_delay_ON();
				KEY_8_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('8');
				break;
		case 70: //F9
				KEY_6_SeagaN =1;
				motor_delay_ON();
				KEY_6_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('6');

				KEY_0_SeagaN =1;
				motor_delay_ON();
				KEY_0_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('0');
				
				KEY_9_SeagaN =1;
				motor_delay_ON();
				KEY_9_SeagaN =0;
				motor_delay_OFF();
				//Debug_PutChar('9');
				break;
	
		default :	break;

	}
#endif

#ifdef SEGA_ALPHANUMERIC_MACHINE	
	switch(gucMCMotorNumber)
	{
		case 1: //A1
				
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;				
				motor_delay_OFF();
				
				KEY_1_Seaga_A=0x01;
				motor_delay_ON();
				KEY_1_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 2: //A2
			
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
                
				KEY_2_Seaga_A=0x01;
				motor_delay_ON();
				KEY_2_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;

		case 3: //A3
				
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
                
				KEY_3_Seaga_A=0x01;
				motor_delay_ON();
				KEY_3_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;
	
		case 4: //A4
				
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_4_Seaga_A=0x01;
				motor_delay_ON();
				KEY_4_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;

		case 5: //A5
				
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_5_Seaga_A=0x01;
				motor_delay_ON();
				KEY_5_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;

		case 6: //A6
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_6_Seaga_A=0x01;
				motor_delay_ON();
				KEY_6_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;

		case 7: //A7
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_7_Seaga_A=0x01;
				motor_delay_ON();
				KEY_7_Seaga_A=0x00;
				motor_delay_OFF();
			
				break;

		case 8: //A8
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_8_Seaga_A=0x01;
				motor_delay_ON();
				KEY_8_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;

		case 9: //A9
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_9_Seaga_A=0x01;
				motor_delay_ON();
				KEY_9_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		case 10: //A10
				KEY_A_Seaga_A=0x01;
				motor_delay_ON();
				KEY_A_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_10_Seaga_A=0x01;
				motor_delay_ON();
				KEY_10_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 11: //B1
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_1_Seaga_A=0x01;
				motor_delay_ON();
				KEY_1_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 12: //B2
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_2_Seaga_A=0x01;
				motor_delay_ON();
				KEY_2_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 13: //B3
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_3_Seaga_A=0x01;
				motor_delay_ON();
				KEY_3_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 14://B4
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_4_Seaga_A=0x01;
				motor_delay_ON();
				KEY_4_Seaga_A=0x00;
				motor_delay_OFF(); 
				break;
	
		case 15://B5
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_5_Seaga_A=0x01;
				motor_delay_ON();
				KEY_5_Seaga_A=0x00;
				motor_delay_OFF(); 
				break;

		case 16: //B6
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_6_Seaga_A=0x01;
				motor_delay_ON();
				KEY_6_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 17: //B7
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_7_Seaga_A=0x01;
				motor_delay_ON();
				KEY_7_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 18: //B8
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_8_Seaga_A=0x01;
				motor_delay_ON();
				KEY_8_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 19: //B9
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_9_Seaga_A=0x01;
				motor_delay_ON();
				KEY_9_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;
		case 20: //B10
				KEY_B_Seaga_A=0x01;
				motor_delay_ON();
				KEY_B_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_10_Seaga_A=0x01;
				motor_delay_ON();
				KEY_10_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;

		case 21: //C1
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_1_Seaga_A=0x01;
				motor_delay_ON();
				KEY_1_Seaga_A=0x00;
				motor_delay_OFF();

				break;

		case 22: //C2
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_2_Seaga_A=0x01;
				motor_delay_ON();
				KEY_2_Seaga_A=0x00;
				motor_delay_OFF();
				break;
	
		case 23: //C3
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_3_Seaga_A=0x01;
				motor_delay_ON();
				KEY_3_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 24: //C4
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_4_Seaga_A=0x01;
				motor_delay_ON();
				KEY_4_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 25: //C5
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_5_Seaga_A=0x01;
				motor_delay_ON();
				KEY_5_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		

		case 26: //C6
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_6_Seaga_A=0x01;
				motor_delay_ON();
				KEY_6_Seaga_A=0x00;
				motor_delay_OFF();
				
				break;

		case 27: //C7
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_7_Seaga_A=0x01;
				motor_delay_ON();
				KEY_7_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 28: //C8
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_8_Seaga_A=0x01;
				motor_delay_ON();
				KEY_8_Seaga_A=0x00;
				motor_delay_OFF();
				break;
	
		case 29: //C9
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_9_Seaga_A=0x01;
				motor_delay_ON();
				KEY_9_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		case 30: //C10
				KEY_C_Seaga_A=0x01;
				motor_delay_ON();
				KEY_C_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_10_Seaga_A=0x01;
				motor_delay_ON();
				KEY_10_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 31: //D1

				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();

				KEY_1_Seaga_A=0x01;
				motor_delay_ON();
				KEY_1_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 32: //D2

				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();

				KEY_2_Seaga_A=0x01;
				motor_delay_ON();
				KEY_2_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 33://D3

				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();

				KEY_3_Seaga_A=0x01;
				motor_delay_ON();
				KEY_3_Seaga_A=0x00;
				motor_delay_OFF(); 
				break;

		case 34: //D4
				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_4_Seaga_A=0x01;
				motor_delay_ON();
				KEY_4_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 35: //D5
				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_5_Seaga_A=0x01;
				motor_delay_ON();
				KEY_5_Seaga_A=0x00;
				motor_delay_OFF();
				break;
	
		case 36: //D6
				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_6_Seaga_A=0x01;
				motor_delay_ON();
				KEY_6_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 37: //D7
				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_7_Seaga_A=0x01;
				motor_delay_ON();
				KEY_7_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 38: //D8
				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_8_Seaga_A=0x01;
				motor_delay_ON();
				KEY_8_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 39: //D9
				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_9_Seaga_A=0x01;
				motor_delay_ON();
				KEY_9_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		case 40: //D10
				KEY_D_Seaga_A=0x01;
				motor_delay_ON();
				KEY_D_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_10_Seaga_A=0x01;
				motor_delay_ON();
				KEY_10_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		
		case 41: //E1
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_1_Seaga_A=0x01;
				motor_delay_ON();
				KEY_1_Seaga_A=0x00;
				motor_delay_OFF();
				break;
	
		case 42: //E2
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_2_Seaga_A=0x01;
				motor_delay_ON();
				KEY_2_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 43: //E3
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_3_Seaga_A=0x01;
				motor_delay_ON();
				KEY_3_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 44: //E4
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_4_Seaga_A=0x01;
				motor_delay_ON();
				KEY_4_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 45: //E5
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_5_Seaga_A=0x01;
				motor_delay_ON();
				KEY_5_Seaga_A=0x00;
				motor_delay_OFF();
				break;


		case 46: //E6
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_6_Seaga_A=0x01;
				motor_delay_ON();
				KEY_6_Seaga_A=0x00;
				motor_delay_OFF();
				break;
	
		case 47: //E7
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_7_Seaga_A=0x01;
				motor_delay_ON();
				KEY_7_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 48: //E8
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_8_Seaga_A=0x01;
				motor_delay_ON();
				KEY_8_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 49: //E9
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_9_Seaga_A=0x01;
				motor_delay_ON();
				KEY_9_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		case 50: //E10
				KEY_E_Seaga_A=0x01;
				motor_delay_ON();
				KEY_E_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_10_Seaga_A=0x01;
				motor_delay_ON();
				KEY_10_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		case 51: //F1
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_1_Seaga_A=0x01;
				motor_delay_ON();
				KEY_1_Seaga_A=0x00;
				motor_delay_OFF();
				break;
	
		case 52: //F2
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_2_Seaga_A=0x01;
				motor_delay_ON();
				KEY_2_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 53: //F3
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_3_Seaga_A=0x01;
				motor_delay_ON();
				KEY_3_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 54: //F4
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_4_Seaga_A=0x01;
				motor_delay_ON();
				KEY_4_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 55: //F5
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_5_Seaga_A=0x01;
				motor_delay_ON();
				KEY_5_Seaga_A=0x00;
				motor_delay_OFF();
				break;


		case 56: //F6
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_6_Seaga_A=0x01;
				motor_delay_ON();
				KEY_6_Seaga_A=0x00;
				motor_delay_OFF();
				break;
	
		case 57: //F7
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_7_Seaga_A=0x01;
				motor_delay_ON();
				KEY_7_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 58: //F8
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_8_Seaga_A=0x01;
				motor_delay_ON();
				KEY_8_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		case 59: //F9
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_9_Seaga_A=0x01;
				motor_delay_ON();
				KEY_9_Seaga_A=0x00;
				motor_delay_OFF();
				break;

		case 60: //F10
				KEY_F_Seaga_A=0x01;
				motor_delay_ON();
				KEY_F_Seaga_A=0x00;
				motor_delay_OFF();
				
				KEY_10_Seaga_A=0x01;
				motor_delay_ON();
				KEY_10_Seaga_A=0x00;
				motor_delay_OFF();
				break;
		
		default :	break;

	}
#endif

#ifdef VENDIMAN_XY_MACHINE	
    for(unsigned long x=0;x<500000;x++);
	switch(gucMCMotorNumber)
	{

        case 1: //A1 ,101



                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 2: //A2

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 3: //A3

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 4: //A4

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 5: //A5

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 6: //A6
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_6_VendiXY=0x01;
                motor_delay_ON();
                KEY_6_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 7: //A7
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_7_VendiXY=0x01;
                motor_delay_ON();
                KEY_7_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 8: //A8
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_8_VendiXY=0x01;
                motor_delay_ON();
                KEY_8_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 9: //A9
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                KEY_9_VendiXY=0x01;
                motor_delay_ON();
                KEY_9_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 10: //110
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 11: //B1 111
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 12: //B2 112
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 13: //B3 113
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 14://B4 114
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 15://B5 115
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 16: //B6 116
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_6_VendiXY=0x01;
                motor_delay_ON();
                KEY_6_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 17: //B7 117 
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_7_VendiXY=0x01;
                motor_delay_ON();
                KEY_7_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 18: //B8 118 
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_8_VendiXY=0x01;
                motor_delay_ON();
                KEY_8_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 19: //B9 119 
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_9_VendiXY=0x01;
                motor_delay_ON();
                KEY_9_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 20: //B10 210
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;



                motor_delay_OFF();

                break;


        case 21: //C1 121
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 22: //C2 122
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 23: //C3 123
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 24: //C4 124
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 25: //C5 125
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();
                break;


        case 26: //C6 126
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_6_VendiXY=0x01;
                motor_delay_ON();
                KEY_6_VendiXY=0x00;
                motor_delay_OFF();

                break;

        case 27: //C7 127 
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_7_VendiXY=0x01;
                motor_delay_ON();
                KEY_7_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 28: //C8 128
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_8_VendiXY=0x01;
                motor_delay_ON();
                KEY_8_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 29: //C9 129
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();

                KEY_9_VendiXY=0x01;
                motor_delay_ON();
                KEY_9_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 30: //C10 130
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 31: //D1 131

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 32: //D2 132

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 33://D3 133

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 34: //D4 134
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 35: //D5 135
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 36: //D6 136
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_6_VendiXY=0x01;
                motor_delay_ON();
                KEY_6_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 37: //D7 137
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_7_VendiXY=0x01;
                motor_delay_ON();
                KEY_7_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 38: //D8 138
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_8_VendiXY=0x01;
                motor_delay_ON();
                KEY_8_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 39: //D9 139
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();

                KEY_9_VendiXY=0x01;
                motor_delay_ON();
                KEY_9_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 40: //D10 140 
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 41: //E1 141
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 42: //E2 142
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 43: //E3 143
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 44: //E4 144
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 45: //E5 145
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();
                break;


        case 46: //E6 146
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_6_VendiXY=0x01;
                motor_delay_ON();
                KEY_6_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 47: //E7 147
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_7_VendiXY=0x01;
                motor_delay_ON();
                KEY_7_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 48: //E8 148
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_8_VendiXY=0x01;
                motor_delay_ON();
                KEY_8_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 49: //E9 149
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();

                KEY_9_VendiXY=0x01;
                motor_delay_ON();
                KEY_9_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 50: //E10 150
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 51: //F1 151
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 52: //F2 152
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_2_VendiXY=0x01;
                motor_delay_ON();
                KEY_2_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 53: //F3 153
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_3_VendiXY=0x01;
                motor_delay_ON();
                KEY_3_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 54: //F4 154
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_4_VendiXY=0x01;
                motor_delay_ON();
                KEY_4_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 55: //F5 155
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();
                break;


        case 56: //F6 156 
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_6_VendiXY=0x01;
                motor_delay_ON();
                KEY_6_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 57: //F7 157
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_7_VendiXY=0x01;
                motor_delay_ON();
                KEY_7_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 58: //F8 158 
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_8_VendiXY=0x01;
                motor_delay_ON();
                KEY_8_VendiXY=0x00;
                motor_delay_OFF();
                    break;

        case 59: //F9 159
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_5_VendiXY=0x01;
                motor_delay_ON();
                KEY_5_VendiXY=0x00;
                motor_delay_OFF();

                KEY_9_VendiXY=0x01;
                motor_delay_ON();
                KEY_9_VendiXY=0x00;
                motor_delay_OFF();
                break;

        case 60: //F10 160 
                KEY_1_VendiXY=0x01;
                motor_delay_ON();
                KEY_1_VendiXY=0x00;
                motor_delay_OFF();

                KEY_6_VendiXY=0x01;
                motor_delay_ON();
                KEY_6_VendiXY=0x00;
                motor_delay_OFF();

                KEY_0_VendiXY=0x01;
                motor_delay_ON();
                KEY_0_VendiXY=0x00;
                motor_delay_OFF();
                break;

        default :	break;

	}
#endif
    
#ifdef TCN_COMBO_WHITE_MACHINE	
	switch(gucMCMotorNumber)
	{
		case 1: //01
		
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				motor_delay_OFF();

				break;

		case 2: //02
			
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				motor_delay_OFF();
				
				break;

		case 3: //03
				
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				motor_delay_OFF();
				
				break;
	
		case 4: //04
				
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				motor_delay_OFF();
				
				break;

		case 5: //05
				
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				motor_delay_OFF();
				
				break;

		case 6: //06
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				motor_delay_OFF();
				
				break;

		case 7: //07
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_7_TCN=0x01;
				motor_delay_ON();
				KEY_7_TCN=0x00;
				motor_delay_OFF();
			
				break;

		case 8: //08
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_8_TCN=0x01;
				motor_delay_ON();
				KEY_8_TCN=0x00;
				motor_delay_OFF();
				
				break;

		case 9: //09
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_9_TCN=0x01;
				motor_delay_ON();
				KEY_9_TCN=0x00;
				motor_delay_OFF();
				break;

		case 10: //10
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				motor_delay_OFF();
				break;

		case 11: //11
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				motor_delay_OFF();
				break;

		case 12: //12
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_ON();
					
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				motor_delay_OFF();
				break;

		case 13: //13
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				motor_delay_OFF();
				break;

		case 14://14
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				motor_delay_OFF();
				break;
	
		case 15://15
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				motor_delay_OFF();
				break;

		case 16: //16
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				motor_delay_OFF();
				break;

		case 17: //17
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_7_TCN=0x01;
				motor_delay_ON();
				KEY_7_TCN=0x00;
				motor_delay_OFF();
				break;

		case 18: //18
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_8_TCN=0x01;
				motor_delay_ON();
				KEY_8_TCN=0x00;
				motor_delay_OFF();
				break;
		case 19: //19
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_9_TCN=0x01;
				motor_delay_ON();
				KEY_9_TCN=0x00;
				motor_delay_OFF();
				break;

		case 20: //20 
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				motor_delay_OFF();
				
				break;
		

		case 21: //21
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				motor_delay_OFF();

				break;

		case 22: //22
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				motor_delay_OFF();
				break;
	
		case 23: //23
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				motor_delay_OFF();
				break;

		case 24: //24
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				motor_delay_OFF();
				break;

		case 25: //25
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				motor_delay_OFF();
				break;
		

		case 26: //26
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				motor_delay_OFF();
				
				break;

		case 27: //27
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_7_TCN=0x01;
				motor_delay_ON();
				KEY_7_TCN=0x00;
				motor_delay_OFF();
				break;

		case 28: //28
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_8_TCN=0x01;
				motor_delay_ON();
				KEY_8_TCN=0x00;
				motor_delay_OFF();
				break;
	
		case 29: //29
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_9_TCN=0x01;
				motor_delay_ON();
				KEY_9_TCN=0x00;
				motor_delay_OFF();
				break;

		case 30: //30 
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				motor_delay_OFF();
				break;

		case 31: //31

				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				motor_delay_OFF();
				break;

		case 32: //32
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				motor_delay_OFF();
				break;

		case 33://33
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				motor_delay_OFF();
				break;

		case 34: //34
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				motor_delay_OFF();
				break;

		case 35: //35
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				motor_delay_OFF();
				break;
	
		case 36: //36
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				motor_delay_OFF();
				break;

		case 37: //37
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_7_TCN=0x01;
				motor_delay_ON();
				KEY_7_TCN=0x00;
				motor_delay_OFF();
				break;

		case 38: //38
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_8_TCN=0x01;
				motor_delay_ON();
				KEY_8_TCN=0x00;
				motor_delay_OFF();
				break;

		case 39: //39
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_9_TCN=0x01;
				motor_delay_ON();
				KEY_9_TCN=0x00;
				motor_delay_OFF();
				break;
		
		case 40: // 40
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				motor_delay_OFF();
				break;
		
		case 41: //41
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				motor_delay_OFF();
				break;
	
		case 42: //42
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				motor_delay_OFF();
				break;

		case 43: //43
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				motor_delay_OFF();
				break;

		case 44: //44
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				motor_delay_OFF();
				break;

		case 45: //45
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				motor_delay_OFF();
				break;


		case 46: //E6
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				motor_delay_OFF();
				break;
	
		case 47: //47
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_7_TCN=0x01;
				motor_delay_ON();
				KEY_7_TCN=0x00;
				motor_delay_OFF();
				break;

		case 48: //48
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_8_TCN=0x01;
				motor_delay_ON();
				KEY_8_TCN=0x00;
				motor_delay_OFF();
				break;

		case 49: //49
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_9_TCN=0x01;
				motor_delay_ON();
				KEY_9_TCN=0x00;
				motor_delay_OFF();
				break;

		case 50: //50
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				motor_delay_OFF();
				break;

		case 51: //51
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				motor_delay_OFF();
				break;
	
		case 52: //52
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				motor_delay_OFF();
				break;

		case 53: //53
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				motor_delay_OFF();
				break;

		case 54: //54
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				motor_delay_OFF();
				break;

		case 55: //55
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				motor_delay_OFF();
				break;


		case 56: //56
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				motor_delay_OFF();
				break;
	
		case 57: //57
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_7_TCN=0x01;
				motor_delay_ON();
				KEY_7_TCN=0x00;
				motor_delay_OFF();
				break;

		case 58: //58
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_8_TCN=0x01;
				motor_delay_ON();
				KEY_8_TCN=0x00;
				motor_delay_OFF();
				break;
		case 59: //F9
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_9_TCN=0x01;
				motor_delay_ON();
				KEY_9_TCN=0x00;
				motor_delay_OFF();
				break;

		case 60: // 60
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				motor_delay_OFF();
				break;
		case 61: // 61
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_1_TCN=0x01;
				motor_delay_ON();
				KEY_1_TCN=0x00;
				motor_delay_OFF();
				break;				
		case 62: // 62
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_2_TCN=0x01;
				motor_delay_ON();
				KEY_2_TCN=0x00;
				motor_delay_OFF();
				break;
		case 63: // 63
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_3_TCN=0x01;
				motor_delay_ON();
				KEY_3_TCN=0x00;
				motor_delay_OFF();
				break;				
		case 64: // 64
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_4_TCN=0x01;
				motor_delay_ON();
				KEY_4_TCN=0x00;
				motor_delay_OFF();
				break;
		case 65: // 65
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_5_TCN=0x01;
				motor_delay_ON();
				KEY_5_TCN=0x00;
				motor_delay_OFF();
				break;
		case 66: // 66
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				motor_delay_OFF();
				break;
		case 67: // 67
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_7_TCN=0x01;
				motor_delay_ON();
				KEY_7_TCN=0x00;
				motor_delay_OFF();
				break;
		case 68: // 68
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_8_TCN=0x01;
				motor_delay_ON();
				KEY_8_TCN=0x00;
				motor_delay_OFF();
				break;
//		case 68: // 68
//				KEY_6_TCN=0x01;
//				motor_delay_ON();
//				KEY_6_TCN=0x00;
//				
//				motor_delay_OFF();
//					
//				KEY_8_TCN=0x01;
//				motor_delay_ON();
//				KEY_8_TCN=0x00;
//				motor_delay_OFF();
//				break;
		case 69: // 69
				KEY_6_TCN=0x01;
				motor_delay_ON();
				KEY_6_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_9_TCN=0x01;
				motor_delay_ON();
				KEY_9_TCN=0x00;
				motor_delay_OFF();
				break;
		case 70: // 70
				KEY_7_TCN=0x01;
				motor_delay_ON();
				KEY_7_TCN=0x00;
				
				motor_delay_OFF();
					
				KEY_0_TCN=0x01;
				motor_delay_ON();
				KEY_0_TCN=0x00;
				motor_delay_OFF();
				break;
																																						
		default :	break;

	}
#endif
    
#ifdef VEND_STOP_BLACK_MACHINE
    
	switch(gucMCMotorNumber )
	{
		case 1: //10 ,000
					
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_0_VendStop=0x01;
				motor_delay_ON();
				KEY_0_VendStop=0x00;

				motor_delay_OFF();

				break;

		case 2: //11,001
			
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();
				
				break;

		case 3: //12,002
				
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;
				motor_delay_OFF();
				
				break;
	
		case 4: //13,003
				
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;
				motor_delay_OFF();
				
				break;

		case 5: //14,004
				
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;
				motor_delay_OFF();
				
				break;

		case 6: //15,005
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();
				
				break;

		case 7: //16,006
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;
				motor_delay_OFF();
			
				break;

		case 8: //17,007
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_7_VendStop=0x01;
				motor_delay_ON();
				KEY_7_VendStop=0x00;
				motor_delay_OFF();
				
				break;

		case 9: //18,008
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_8_VendStop=0x01;
				motor_delay_ON();
				KEY_8_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 10: //19
				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;

				motor_delay_OFF();

				KEY_9_VendStop=0x01;
				motor_delay_ON();
				KEY_9_VendStop=0x00;
				motor_delay_OFF();

				break;

		case 11: //20
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_0_VendStop=0x01;
				motor_delay_ON();
				KEY_0_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 12: //21
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 13: //22
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();
				break;

		case 14://23
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();
				break;
	
		case 15://24
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 16: //25
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();
				break;

		case 17: //26
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 18: //27
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_7_VendStop=0x01;
				motor_delay_ON();
				KEY_7_VendStop=0x00;

				motor_delay_OFF();
				break;

		case 19: //28
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();
		
				KEY_8_VendStop=0x01;
				motor_delay_ON();
				KEY_8_VendStop=0x00;

				motor_delay_OFF();
				
				break;
		

		case 20: //29
				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;

				motor_delay_OFF();

				KEY_9_VendStop=0x01;
				motor_delay_ON();
				KEY_9_VendStop=0x00;
				motor_delay_OFF();

				break;

		case 21: //30
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_0_VendStop=0x01;
				motor_delay_ON();
				KEY_0_VendStop=0x00;
				motor_delay_OFF();
				break;
	
		case 22: //31
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 23: //32
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 24: //33
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;
				motor_delay_OFF();
				break;
		

		case 25: //34
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();
				
				break;

		case 26: //35
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 27: //36
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();
				break;
	
		case 28: //37
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_7_VendStop=0x01;
				motor_delay_ON();
				KEY_7_VendStop=0x00;

				motor_delay_OFF();
				break;

		case 29: //38
				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_8_VendStop=0x01;
				motor_delay_ON();
				KEY_8_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 30: //39

				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;

				motor_delay_OFF();

				KEY_9_VendStop=0x01;
				motor_delay_ON();
				KEY_9_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 31: //40

				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_0_VendStop=0x01;
				motor_delay_ON();
				KEY_0_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 32://41

				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;
 				motor_delay_OFF();
				break;

		case 33: //42
				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 34: //43
				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;
				motor_delay_OFF();
				break;
	
		case 35: //44
				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 36: //45
				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 37: //46
				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 38: //47
				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_7_VendStop=0x01;
				motor_delay_ON();
				KEY_7_VendStop=0x00;
				motor_delay_OFF();
				break;
		
		case 39: //48
				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_8_VendStop=0x01;
				motor_delay_ON();
				KEY_8_VendStop=0x00;
				motor_delay_OFF();
				break;
		
		case 40: //49
				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;

				motor_delay_OFF();

				KEY_9_VendStop=0x01;
				motor_delay_ON();
				KEY_9_VendStop=0x00;

				motor_delay_OFF();
				break;
	
		case 41: //50
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_0_VendStop=0x01;
				motor_delay_ON();
				KEY_0_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 42: //51
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 43: //52
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 44: //53
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;
				motor_delay_OFF();
				break;


		case 45: //54
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;
				motor_delay_OFF();
				break;
	
		case 46: //55
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 47: //56
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 48: //57
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_7_VendStop=0x01;
				motor_delay_ON();
				KEY_7_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 49: //58
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_8_VendStop=0x01;
				motor_delay_ON();
				KEY_8_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 50: //59
				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;

				motor_delay_OFF();

				KEY_9_VendStop=0x01;
				motor_delay_ON();
				KEY_9_VendStop=0x00;
				motor_delay_OFF();
				break;
	
		case 51: //60
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_0_VendStop=0x01;
				motor_delay_ON();
				KEY_0_VendStop=0x00;

				motor_delay_OFF();
				break;

		case 52: //61
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_1_VendStop=0x01;
				motor_delay_ON();
				KEY_1_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 53: //62
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_2_VendStop=0x01;
				motor_delay_ON();
				KEY_2_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 54: //63
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_3_VendStop=0x01;
				motor_delay_ON();
				KEY_3_VendStop=0x00;
				motor_delay_OFF();
				break;


		case 55: //64
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_4_VendStop=0x01;
				motor_delay_ON();
				KEY_4_VendStop=0x00;
				motor_delay_OFF();
				break;
	
		case 56: //65
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_5_VendStop=0x01;
				motor_delay_ON();
				KEY_5_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 57: //66
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 58: //67
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_7_VendStop=0x01;
				motor_delay_ON();
				KEY_7_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 59: //68
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_8_VendStop=0x01;
				motor_delay_ON();
				KEY_8_VendStop=0x00;
				motor_delay_OFF();
				break;
		
		case 60: //69
				KEY_6_VendStop=0x01;
				motor_delay_ON();
				KEY_6_VendStop=0x00;

				motor_delay_OFF();

				KEY_9_VendStop=0x01;
				motor_delay_ON();
				KEY_9_VendStop=0x00;
				motor_delay_OFF();
				break;

		case 61: //70
				KEY_7_VendStop=0x01;
				motor_delay_ON();
				KEY_7_VendStop=0x00;

				motor_delay_OFF();

				KEY_0_VendStop=0x01;
				motor_delay_ON();
				KEY_0_VendStop=0x00;
				motor_delay_OFF();
				break;

		default :	break;

	}
#endif

}
unsigned char Hex_to_char(unsigned char temp)
{
	if(temp==0x00)
	{
		return(temp+0x30);
	}
	else if(temp>=0x01 && temp<=0x09)
	{
		return(temp+0x30);
	}
	else if(temp==0x0A)
	{
		return('A');
	}
	else if(temp==0x0B)
	{
		return('B');
	}
	else if(temp==0x0C)
	{
		return('C');
	}
	else if(temp==0x0D)
	{
		return('D');
	}
	else if(temp==0x0E)
	{
		return('E');
	}
	else if(temp==0x0F)
	{
		return('F');
	}
	else
	{
		return(0xFF);
	}
}
