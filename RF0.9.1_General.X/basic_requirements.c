#include "basic_requirements.h"



void pin_initilize(void)
{
        /**
    LATx registers
    */
    LATE = 0x00;
    LATD = 0x00;
    LATA = 0x00;
    LATB = 0x00;
    LATC = 0x00;

    /**
    TRISx registers
    */
    TRISE = 0x00;
    TRISA = 0x00;
    TRISB = 0x00;
    TRISC = 0x00;
    TRISD = 0x00;
    TRISDbits.TRISD3 = 1;      //BLE Status Pin
    TRISAbits.TRISA0 = 1;
    TRISAbits.TRISA3 = 0;
    

    /**
    ANSELx registers
    */
    ANSELD = 0x00;
    ANSELC = 0x00;
    ANSELB = 0x00;
    ANSELE = 0x00;
    ANSELA = 0x00;

    /**
    WPUx registers
    */
    WPUB = 0x00;
    INTCON2bits.nRBPU = 1;
}


//
//unsigned char Hex_to_char(unsigned char temp)
//{
//	if(temp==0x00)
//	{
//		return(temp+0x30);
//	}
//	else if(temp>=0x01 && temp<=0x09)
//	{
//		return(temp+0x30);
//	}
//	else if(temp==0x0A)
//	{
//		return('A');
//	}
//	else if(temp==0x0B)
//	{
//		return('B');
//	}
//	else if(temp==0x0C)
//	{
//		return('C');
//	}
//	else if(temp==0x0D)
//	{
//		return('D');
//	}
//	else if(temp==0x0E)
//	{
//		return('E');
//	}
//	else if(temp==0x0F)
//	{
//		return('F');
//	}
//	else
//	{
//		return(0xFF);
//	}
//}
//
