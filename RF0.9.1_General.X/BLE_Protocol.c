#include "basic_requirements.h"



unsigned char gucCash_SalesData_Buff[2500];

unsigned char gucRx_Product_Code[5];
unsigned char gucRx_Payment_Code[8];
unsigned char gucProduct_Price[6];
unsigned char gucRevert_String[10];

unsigned int guiBLERecPrice;
unsigned char rev_amount_H,rev_amount_L;


unsigned int guiRelayPressCounter=0,guiSendResponseCounter=0;
unsigned char guc_HW_INfoSnd_OnceFLG=0,gucMachineReadyToVend;


unsigned int zLen=0;

unsigned char guiLastBLE_RxData=0;
unsigned int guiCashstring_Len=0;
unsigned char gucCashstring_Len_inString[8];

unsigned int  guiBeginSessionTimerCounter=0;
unsigned char gucBeginSessionTimerFlag=0;

void IsBLEConnected()
{
    if(dBLE_STATUS_PIN==0)
    {
        if(!guc_HW_INfoSnd_OnceFLG)
        {
            if((gucMDB_ReaderEnable_Flag) && (gucMachineReadyToVend))
            {
                
                __delay_ms(500);
                
               
                
                guc_St_RxRF_flag=0;// flag high to show correct frame is received  
                guiRxRF_Buffer_Index=0;
                
                        
//                guiStar_buff_index=0;
//                guiDollor_buff_index=0;
                
                BLE_PutString("\r\n");
                
                gucVendStartFlag=0;
                        
                gucSendMachineReadyFlag=1;
                gucMachineReadySendCount=0;
                guiMachineReadyCounter=20000;
            }
            else
            {
                __delay_ms(500);
                
                guc_St_RxRF_flag=0;// flag high to show correct frame is received  
                guiRxRF_Buffer_Index=0;
                
//                guiStar_buff_index=0;
//                guiDollor_buff_index=0;
                BLE_PutString("Machine Not Ready\r\n");
            }
                
            guc_HW_INfoSnd_OnceFLG=1;           
        }         
    }
    else
    {
        if(guc_HW_INfoSnd_OnceFLG)
        {
            
            guc_HW_INfoSnd_OnceFLG=0;           
            
            gucSendMachineReadyFlag=0;
            guiMachineReadyCounter=0;
            gucMachineReadySendCount=0;
            
            ucVendingTimerExpiredFlg=0;
            uc_VndReqstTimerFLG=0;
            ui_VndReqstTimerFCount=0;
            
            
//            guiStar_buff_index=0;
//            guiDollor_buff_index=0;
        }
    }
}


void BLE_Communication()
{
    unsigned char key=0;
    unsigned char gucCompartmentOKFlag=0,gucProductPriceOKFlag=0;
    
    
    if(gucRxRF_Fr_rec_Flag==1)
    {
        for(int ix=0;ix<MaxChars;ix++)
        {
            gucRxRF_Buffer_copy[ix]='\0';
        }
        
        for(int ix=0;ix<guiRxRF_Buffer_Index;ix++)
        {
            gucRxRF_Buffer_copy[ix]=gucRxRF_Buffer[ix];
        }
        
        guc_St_RxRF_flag=0;// flag high to show correct frame is received  
        guiRxRF_Buffer_Index=0;
        gucRxRF_Fr_rec_Flag=0;
        
//        BLE_PutString(gucRxRF_Buffer_copy);
        
        if((gucRxRF_Buffer_copy[0]=='$'))//dddddd && (gucRxRF_Buffer_copy[2]=='$'))
        {
            for (int i=0;i<=9;i++)
            {

                char suc=compare_strings(Dollar_Array[i],gucRxRF_Buffer_copy);
               
                if (suc==0)
                {
                    switch(i)
                    {
                        case(0): // Cash & EJB error

                                     for ( zLen=0;gucCash_SalesData_Buff[zLen]!='#';zLen++)
                                        {
                                            if(gucCash_SalesData_Buff[zLen]!='-')
                                            {
                                                BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                            }
                                            else if(gucCash_SalesData_Buff[zLen]=='-')
                                            {
                                                BLE_PutChar('-');
                                                zLen++;
                                                while(gucCash_SalesData_Buff[zLen]!='-')
                                                {
                                                    zLen++;
                                                }
                                            }
                                        }
                                       BLE_PutChar('#');
                                       zLen++;
                                       for(;gucCash_SalesData_Buff[zLen]!=0x01;zLen++)
                                       {
                                           BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                       }
                                       BLE_PutString("\r\n");

                                break;

                        case(1): // Cash and soldout

                                    for ( zLen=0;gucCash_SalesData_Buff[zLen]!='#';zLen++)
                                    {
                                        if(gucCash_SalesData_Buff[zLen]!='-')
                                        {
    //                                        printf("%c ", arr[zLen]);
                                            BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                        }
                                        else if(gucCash_SalesData_Buff[zLen]=='-')
                                        {
    //                                        printf("%c", '-');
                                            BLE_PutChar('-');
                                            zLen++;
                                            while(gucCash_SalesData_Buff[zLen]!='-')
                                            {
                                                zLen++;
                                            }
                                        }
                                    }	
                                    BLE_PutString("#S-*\r\n");
                                     break;
                        case(2)://cash & Temp  

                                    for ( zLen=0;gucCash_SalesData_Buff[zLen]!='#';zLen++)
                                    {
                                        if(gucCash_SalesData_Buff[zLen]!='-')
                                        {
    //                                        printf("%c ", arr[zLen]);
                                            BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                        }
                                        else if(gucCash_SalesData_Buff[zLen]=='-')
                                        {
    //                                        printf("%c", '-');
                                            BLE_PutChar('-');
                                            zLen++;
                                            while(gucCash_SalesData_Buff[zLen]!='-')
                                            {
                                                zLen++;
                                            }
                                        }
                                    }	
                                    BLE_PutString("#T-*\r\n");
                                    break;
                        case(3): // Cash & Motor Jam  Case

                                    for ( zLen=0;gucCash_SalesData_Buff[zLen]!='#';zLen++)
                                    {
                                        if(gucCash_SalesData_Buff[zLen]!='-')
                                        {
    //                                        printf("%c ", arr[zLen]);
                                            BLE_PutChar(gucCash_SalesData_Buff[zLen]);
                                        }
                                        else if(gucCash_SalesData_Buff[zLen]=='-')
                                        {
    //                                        printf("%c", '-');
                                            BLE_PutChar('-');
                                            zLen++;
                                            while(gucCash_SalesData_Buff[zLen]!='-')
                                            {
                                                zLen++;
                                            }
                                        }
                                    }	
                                    BLE_PutString("#J-*\r\n");
                                    break;

                        case(4)://  DEX initialization
                                    if(!gucDex_init_flag)
                                    {
                                       gucDollorD_Flag=1;
                                    }
                                    else
                                    {
                                        BLE_PutString("DEX is already Running\r\n");
                                    }
                                   break;
                        case(5):// CC data
    //                            BLE_PutString("CC data\r\n");
                                key=Read_EEPROM(0X00);
                                __delay_ms(1);
    //                              Complete_CC_Data();
                                  break;
                        case(6):// Cash  Data with compartment price 
                                key=Read_EEPROM(0X00);
                                __delay_ms(1);
                                key=Read_EEPROM(0X00);
                                if(key==255)
                                {
                                  BLE_PutString("DEX Not Initialized");
                                }
                                else
                                {
                                    BLE_PutString(gucCash_SalesData_Buff);
                                    BLE_PutString("\r\n");
                                }
                                break;


                        case(7): // Ack for read eanable string 
                            
                                    
                                    
                                    gucSendReadEnableFlag=0;
                                    gucReadEnableSendCount=0;
                                  
                                    BLE_PutString("RD\r\n");
                                 
                                break;
                        case(8):// Ack for Machine Readye string
                                gucSendMachineReadyFlag=0;
                                guiMachineReadyCounter=0;
                                break;    
                        case(9):// Device info

                                #ifdef SEGA_NUMERIC_MACHINE
                                    BLE_PutString("Sega Numeric Machine\r\n");
                                    BLE_PutString("Version:0.9.1\r\n");
                                #endif

                                #ifdef SEGA_ALPHANUMERIC_MACHINE
                                    BLE_PutString("Sega AlphaNumeric Machine\r\n");
                                    BLE_PutString("Version:0.9.1\r\n");
                                #endif

                                #ifdef VENDIMAN_XY_MACHINE 
                                    BLE_PutString("Vendiman XY Machine\r\n");
                                    BLE_PutString("Version:0.9.1\r\n");
                                #endif

                                #ifdef TCN_COMBO_WHITE_MACHINE
                                    BLE_PutString("TCN COMBO White Machine\r\n");
                                    BLE_PutString("Version:0.9.1\r\n");
                                #endif
                                    
                                #ifdef VEND_STOP_BLACK_MACHINE
                                    BLE_PutString("VEND STOP BLACK Machine\r\n");
                                    BLE_PutString("Version:0.9.1\r\n");
                                #endif
                                break;
                        default: break;
                    }
    //                break;
                }
            }
        }
        else if ((gucRxRF_Buffer_copy[0]=='*') && (gucRxRF_Buffer_copy[29]=='*') && (gucVendStartFlag==0))
        {
//            BLE_PutStringL(gucStar_buffer,32);
            gucRelayPressCounter=0;
            gucStartRelayPressCounterFlag=0;
            gucCancel_Session=0;
            
            gucAmountCreditedPressKey=0;
                 
            uc_VndReqstTimerFLG=1;
            ui_VndReqstTimerFCount=0;
                                            
            gucVendStartFlag=1;
            gucSendMachineReadyFlag=0;

            // Payment 7digit code
             gucRx_Payment_Code[0]= gucRxRF_Buffer_copy[11];
             gucRx_Payment_Code[1]= gucRxRF_Buffer_copy[6];
             gucRx_Payment_Code[2]= gucRxRF_Buffer_copy[15];
             gucRx_Payment_Code[3]= gucRxRF_Buffer_copy[4];
             gucRx_Payment_Code[4]= gucRxRF_Buffer_copy[13];
             gucRx_Payment_Code[5]= gucRxRF_Buffer_copy[8];
             gucRx_Payment_Code[6]= gucRxRF_Buffer_copy[17];
             gucRx_Payment_Code[7]='\0';

          // product no	
            for(int i=0;i<5;i++)
            {
                gucRx_Product_Code[i]='\0';
            }
            #ifdef SEGA_NUMERIC_MACHINE
                gucRx_Product_Code[0]= '0';
                gucRx_Product_Code[1]= gucRxRF_Buffer_copy[21]+1;
                gucRx_Product_Code[2]= gucRxRF_Buffer_copy[22];
                gucRx_Product_Code[3]= gucRxRF_Buffer_copy[23]+1;	
                gucRx_Product_Code[4]= '\0';
            #endif
           
            #ifdef SEGA_ALPHANUMERIC_MACHINE
                gucRx_Product_Code[0]= '0';
                gucRx_Product_Code[1]= gucRxRF_Buffer_copy[21];
                gucRx_Product_Code[2]= gucRxRF_Buffer_copy[22];
                gucRx_Product_Code[3]= gucRxRF_Buffer_copy[23];	
                gucRx_Product_Code[4]= '\0';
            #endif

            #ifdef VENDIMAN_XY_MACHINE 
                gucRx_Product_Code[0]= '0';
                gucRx_Product_Code[1]= gucRxRF_Buffer_copy[21];
                gucRx_Product_Code[2]= gucRxRF_Buffer_copy[22];
                gucRx_Product_Code[3]= gucRxRF_Buffer_copy[23];	
                gucRx_Product_Code[4]= '\0';
            #endif

            #ifdef TCN_COMBO_WHITE_MACHINE
                gucRx_Product_Code[0]= '0';
                gucRx_Product_Code[1]= gucRxRF_Buffer_copy[21];
                gucRx_Product_Code[2]= gucRxRF_Buffer_copy[22];
                gucRx_Product_Code[3]= gucRxRF_Buffer_copy[23];	
                gucRx_Product_Code[4]= '\0';
            #endif

            #ifdef VEND_STOP_BLACK_MACHINE

            #endif
             
            if(((gucRx_Product_Code[1]>='0') && ((gucRx_Product_Code[1]<='9')))&&
                    ((gucRx_Product_Code[2]>='0') && ((gucRx_Product_Code[2]<='9')))&&
                    ((gucRx_Product_Code[3]>='0') && ((gucRx_Product_Code[3]<='9'))))
            {
                gucCompartmentOKFlag=1;
            }
            

          // prouct price
             gucProduct_Price[0]= gucRxRF_Buffer_copy[24];
             gucProduct_Price[1]= gucRxRF_Buffer_copy[25];
             gucProduct_Price[2]= gucRxRF_Buffer_copy[26];
             gucProduct_Price[3]= gucRxRF_Buffer_copy[27];
             gucProduct_Price[4]= gucRxRF_Buffer_copy[28];
             gucProduct_Price[5]= '\0';
             
             if(((gucProduct_Price[0]>='0') && ((gucProduct_Price[0]<='9')))&&
                    ((gucProduct_Price[1]>='0') && ((gucProduct_Price[1]<='9')))&&
                    ((gucProduct_Price[2]>='0') && ((gucProduct_Price[2]<='9')))&&
                    ((gucProduct_Price[3]>='0') && ((gucProduct_Price[3]<='9')))&&
                    ((gucProduct_Price[4]>='0') && ((gucProduct_Price[4]<='9'))))
             {
                 gucProductPriceOKFlag=1;
             }
         
             
             // revert back
             gucRevert_String[0]= '2';
             gucRevert_String[1]= '9';
             gucRevert_String[2]= '9';
             gucRevert_String[3]= '9';
             gucRevert_String[4]= '9';
             gucRevert_String[5]= '9';
             gucRevert_String[6]= '9';
             gucRevert_String[7]=(gucLastVendStatus);
             gucRevert_String[8]= '\0';
             
             
             if((gucCompartmentOKFlag) && (gucProductPriceOKFlag))
             {
                int b;
                for (int j=0;j<=35;j++)
                {
                    b=compare_strings(&Payment_Code[j][0],gucRx_Payment_Code);
                    if (b==0)
                    {
                        break;
                    }
                }
                if (b==0) 
                {

                    BLE_PutString("ACK_ST\r\n");

                    gucRevert_String[4] = gucRxRF_Buffer_copy[21];
                    gucRevert_String[5] = gucRxRF_Buffer_copy[22];
                    gucRevert_String[6] = gucRxRF_Buffer_copy[23];

                    //rev_amount = atoi(product_price);	
                    guiBLERecPrice = atof(gucProduct_Price);
                    guiBLERecPrice=(unsigned int)((guiBLERecPrice-14));
                    guiBLERecPrice = guiBLERecPrice *100;
                    
//                    BLE_PutString("VNK Price:- ");
//                    BLE_PutChar((guiBLERecPrice/1000)+0x30);
//                    BLE_PutChar(((guiBLERecPrice/100)%10)+0x30);
//                    BLE_PutChar(((guiBLERecPrice/10)%10)+0x30);
//                    BLE_PutChar((guiBLERecPrice%10)+0x30);
//                    BLE_PutString("\r\n");

                    rev_amount_L=guiBLERecPrice & 0xFF;
                    rev_amount_H=guiBLERecPrice>>8;
                    
//                    #ifdef VENDIMAN_XY_MACHINE
//                        for(unsigned long x=0;x<50000;x++);
//                    #endif
                    
//                    BLE_PutString("rev_amount_L Price:- ");
//                    BLE_PutChar((rev_amount_L/1000)+0x30);
//                    BLE_PutChar(((rev_amount_L/100)%10)+0x30);
//                    BLE_PutChar(((rev_amount_L/10)%10)+0x30);
//                    BLE_PutChar((rev_amount_L%10)+0x30);
//                    BLE_PutString("\r\n");
//                    
//                    BLE_PutString("rev_amount_H Price:- ");
//                    BLE_PutChar((rev_amount_H/1000)+0x30);
//                    BLE_PutChar(((rev_amount_H/100)%10)+0x30);
//                    BLE_PutChar(((rev_amount_H/10)%10)+0x30);
//                    BLE_PutChar((rev_amount_H%10)+0x30);
//                    BLE_PutString("\r\n");

                    if(gucMDB_ReaderEnable_Flag==1)
                    {
                        
                        ucPresentMDB_Stage=1;
                        ucResPollStage = BEGIN_SESSION;

                        guiBeginSessionTimerCounter=0;
                        gucBeginSessionTimerFlag=1;
                    }
                    else
                    {
//                        BLE_PutString("MDB RD Disable\r\n");
                        BLE_PutString("Machine Not Ready\r\n");
                    }
                }
                else if (b!=0)
                {	
                    __delay_ms(20);
                    BLE_PutString("527100150");
                    BLE_PutString("\r\n");
                    __delay_ms(27);

                    BLE_PutString("no vend case\r\n");
                    __delay_ms(500);
                    BLE_PutString("no vend case\r\n");
                    __delay_ms(500);
                    BLE_PutString("no vend case\r\n");
                    __delay_ms(500);
                    BLE_PutString("ssn complete\r\n");
                    __delay_ms(500);
                    BLE_PutString("end ssn\r\n");
                    gucVendStartFlag=0;
                    gucSendReadEnableFlag=1;
                    gucReadEnableSendCount=0;
                    guiReadEnableCounter=20000;
                }
             }
        }
        else if ((gucRxRF_Buffer_copy[0]!='*') && (gucRxRF_Buffer_copy[29]!='*') && (gucVendStartFlag==0))
        {
            ucVendingTimerExpiredFlg=0;
            uc_VndReqstTimerFLG=0;
            ui_VndReqstTimerFCount=0;
        }  
    }
}

void Debug_CovertChar_in_ASCIIwithLen(uint8_t *data,int len)
{
	unsigned char tempData,tempHex,tempHex1;
	while (len > 0)
	{
		tempData = *data++;
		tempHex=((tempData & 0xF0) >>4);
		tempHex1=((tempData & 0x0F));
		//			Hex_to_char(tempHex);   //convert the HEX to Character
		//			Hex_to_char(tempHex1);  //convert the Hex to Character
		//Debug_PutChar(Hex_to_char(tempHex));
		//Debug_PutChar(Hex_to_char(tempHex1));
		//Debug_PutChar(' ');
//		BLE_PutChar(Hex_to_char(tempHex));
//		BLE_PutChar(Hex_to_char(tempHex1));
//		BLE_PutChar(' ');
		len--;
	}
}