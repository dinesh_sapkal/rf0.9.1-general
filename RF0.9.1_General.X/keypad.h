/* 
 * 
 * File:   keypad.h
 * Author: User
 *
 * Created on June 28, 2021, 3:00 PM
 */

#ifndef KEYPAD_H
#define	KEYPAD_H


#define SEGA_NUMERIC_MACHINE
//#define SEGA_ALPHANUMERIC_MACHINE
//#define VENDIMAN_XY_MACHINE

//#define TCN_COMBO_WHITE_MACHINE
//#define VEND_STOP_BLACK_MACHINE


#ifdef SEGA_NUMERIC_MACHINE
#define KEY_1_SeagaN	    		LATB7
#define KEY_2_SeagaN    			LATA1 

 
#define KEY_3_SeagaN    			LATB2
#define KEY_4_SeagaN    			LATB3 

#define KEY_5_SeagaN    			LATA4
#define KEY_6_SeagaN    			LATA5
 

#define KEY_7_SeagaN    			LATE0
#define KEY_8_SeagaN    			LATE1


#define KEY_9_SeagaN    			LATE2
#define KEY_0_SeagaN	    		LATC1 

#define KEY_STR_SeagaN   			LATC0
#define KEY_HASH_SeagaN	    		LATC2 
#endif

#ifdef SEGA_ALPHANUMERIC_MACHINE
#define KEY_A_Seaga_A                LATD0
#define KEY_1_Seaga_A                LATD2
#define KEY_2_Seaga_A                LATB1

#define KEY_B_Seaga_A                LATD1
#define KEY_3_Seaga_A                LATC2
#define KEY_4_Seaga_A                LATD4

#define KEY_C_Seaga_A                LATC0
#define KEY_5_Seaga_A                LATE2
#define KEY_6_Seaga_A                LATB0
 
#define KEY_D_Seaga_A                LATC1
#define KEY_7_Seaga_A                LATA5
#define KEY_8_Seaga_A                LATA2

#define KEY_E_Seaga_A                LATE0
#define KEY_9_Seaga_A                LATB2
#define KEY_10_Seaga_A               LATB6

#define KEY_F_Seaga_A                LATE1
#define KEY_G_Seaga_A                LATB3

#define KEY_UP_Seaga_A                LATA1
#define KEY_DOWN_Seaga_A              LATB7
#endif

#ifdef VENDIMAN_XY_MACHINE
#define KEY_1_VendiXY	    		LATB7
#define KEY_2_VendiXY    			LATA1 

 
#define KEY_3_VendiXY    			LATB2
#define KEY_4_VendiXY    			LATB3 

#define KEY_5_VendiXY    			LATA4
#define KEY_6_VendiXY    			LATA5
 

#define KEY_7_VendiXY    			LATE0
#define KEY_8_VendiXY    			LATE1


#define KEY_9_VendiXY   			LATE2
#define KEY_0_VendiXY	    		LATC1 

#define KEY_STR_VendiXY 			LATC0
#define KEY_HASH_VendiXY	    	LATC2 

#define KEY_EXIT_VendiXY	    		LATB0 
#endif

#ifdef TCN_COMBO_WHITE_MACHINE
#define KEY_1_TCN	    		LATB7  //Relay_C1R1
#define KEY_2_TCN    			LATA1  //Relay_C2R1 
#define KEY_3_TCN    			LATB2  //Relay_C3R1

#define KEY_4_TCN    			LATB3  //Relay_C1R2 
#define KEY_5_TCN    			LATA4  //Relay_C2R2 
#define KEY_6_TCN    			LATA5  //Relay_C3R2
 

#define KEY_7_TCN    			LATE0  //Relay_C1R3 
#define KEY_8_TCN    			LATE1  //Relay_C2R3
#define KEY_9_TCN    			LATE2  //Relay_C3R3 

#define KEY_DOT_TCN   			LATB6   //Relay_C1R4
#define KEY_0_TCN	    		LATC1   //Relay_C2R4
#define KEY_CNL_TCN	    		LATB0  //Relay_C3R4
#endif

#ifdef VEND_STOP_BLACK_MACHINE

#define KEY_1_VendStop	    LATC0
#define KEY_2_VendStop    	LATC1

 
#define KEY_3_VendStop		LATC2
#define KEY_4_VendStop		LATE0

#define KEY_5_VendStop		LATE1
#define KEY_6_VendStop		LATE2
 

#define KEY_7_VendStop		LATB3
#define KEY_8_VendStop		LATA4


#define KEY_9_VendStop		LATA5
#define KEY_0_VendStop		LATA1
#endif


extern unsigned char gucAmountCreditedPressKey,gucMDBSessionStarted;

extern unsigned char gucRx_Product_Code[5];
extern unsigned char gucRx_Payment_Code[8];
extern unsigned char gucProduct_Price[6];
extern unsigned char gucLastVendStatus;
extern unsigned char gucRevert_String[10];

extern const unsigned char Procudt_Code[72][6];
 
extern const unsigned char Payment_Code[36][8];
unsigned int compare_strings(const unsigned char a[], unsigned char b[]);


extern void PressMachineKey(unsigned char motor_num);

void  motor_delay_ON();
void motor_delay_OFF();

unsigned char Hex_to_char(unsigned char temp);
#endif	/* KEYPAD_H */

