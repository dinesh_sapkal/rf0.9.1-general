#include "basic_requirements.h"


void TMR0_Initialize(void)
{ 
    // Set TMR0 to the options selected in the User Interface

    //Enable 16bit timer mode before assigning value to TMR0H
    T0CONbits.T08BIT = 0;

    // TMR0H 120; 
    TMR0H = 0x78;

    // TMR0L 255; 
    TMR0L = 0xFF;

    // Clear Interrupt flag before enabling the interrupt
    INTCONbits.TMR0IF = 0;

    // Enabling TMR0 interrupt.
    INTCONbits.TMR0IE = 1;

    // Set Default Interrupt Handler
//    TMR0_SetInterruptHandler(TMR0_DefaultInterruptHandler);

    // T0PS 1:8; T08BIT 16-bit; T0SE Increment_hi_lo; T0CS FOSC/4; TMR0ON enabled; PSA assigned; 
    T0CON = 0x92;
}

void TMR0_StartTimer(void)
{
    // Start the Timer by writing to TMR0ON bit
    T0CONbits.TMR0ON = 1;
}

//void TMR0_StopTimer(void)
//{
//    // Stop the Timer by writing to TMR0ON bit
//    T0CONbits.TMR0ON = 0;
//}
