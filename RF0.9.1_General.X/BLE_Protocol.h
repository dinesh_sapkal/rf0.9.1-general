/* 
 * File:   BLE_Protocol.h
 * Author: User
 *
 * Created on June 28, 2021, 12:23 PM
 */

#ifndef BLE_PROTOCOL_H
#define	BLE_PROTOCOL_H





extern unsigned int guiBLERecPrice;

extern unsigned char gucBLE_Frm_rec_Flag;

extern unsigned char rev_amount_H,rev_amount_L;

extern unsigned int guiRelayPressCounter,guiSendResponseCounter;

extern unsigned char guc_HW_INfoSnd_OnceFLG,gucMachineReadyToVend;
extern unsigned int guiMachineReadyCounter;

extern unsigned char guiLastBLE_RxData;

extern unsigned int  guiBeginSessionTimerCounter;
extern unsigned char gucBeginSessionTimerFlag;

extern void IsBLEConnected(void);
extern void BLE_Communication(void);
extern void Debug_CovertChar_in_ASCIIwithLen(uint8_t *data,int len);

#endif	/* BLE_PROTOCOL_H */

