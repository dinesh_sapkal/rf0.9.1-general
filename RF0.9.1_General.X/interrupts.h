/* 
 * File:   interrupts.h
 * Author: User
 *
 * Created on June 28, 2021, 12:51 PM
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H


#define   MaxChars	31

extern unsigned char gucRxRF_Buffer[MaxChars],gucRxRF_Buffer_copy[MaxChars];
extern unsigned int guiRxRF_Buffer_Index;
extern unsigned char guc_St_RxRF_flag,gucRxRF_Fr_rec_Flag; 

void __interrupt() INTERRUPT_InterruptManager (void);

#endif	/* INTERRUPTS_H */

