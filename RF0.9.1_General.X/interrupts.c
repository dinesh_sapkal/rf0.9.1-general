#include "basic_requirements.h"

unsigned char gucRxRF_Buffer[MaxChars],gucRxRF_Buffer_copy[MaxChars];
unsigned int guiRxRF_Buffer_Index=0;
unsigned char guc_St_RxRF_flag=0,gucRxRF_Fr_rec_Flag;


unsigned char en_rx_buf=0,bluetooth_step=0,Dollar_Rcv_Flg=0;

void __interrupt() INTERRUPT_InterruptManager()
{
    unsigned int tempRXData_1;
    unsigned char bit9th,tempRXData_2,MDBChecksum;
    volatile unsigned char bleRXData=0;
    
    
    //UART1 MDB interrupt
    if(PIE1bits.RC1IE == 1 && PIR1bits.RC1IF == 1) 
    {
        if(1 == RCSTA1bits.OERR)
        {
            // EUSART2 error - restart
//            BLE_PutString("MORR\r\n");
            RCSTA1bits.CREN = 0;
            NOP();
            RCSTA1bits.CREN = 1;

        }
        
        else
        {
            // buffer overruns are ignored
            bit9th = RX9D1;
            tempRXData_1 = bit9th;	
            tempRXData_1 = tempRXData_1<<8;
            guiMDB_RxData =  tempRXData_1 | RCREG1;

            switch (gucMDB_Step)
            {
                case 0 :
                            switch (guiMDB_RxData)
                            {
                                case VMC_ACK_A		:
                                                        switch (ACK_Stage_State)
                                                        {
                                                            case ACK_RCV_JUST_RESET:
                                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                        ACK_Stage_State=0;
                                                                                        break;
                                                            case ACK_RCV_CONFIGDATA:
                                                                                        ACK_Stage_State=0;
                                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                        break;
                                                            case ACK_RCV_PERI_ID:
                                                                                        ACK_Stage_State=0;
                                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                        break;
                                                            case ACK_RCV_START_SESSION:
    //                                                                                    gucStartRelayPressCounterFlag=1;
    //                                                                                    gucRelayPressCounter=0;
                                                                                        BLE_PutString("start ssn \r\n");
                                                                                        gucRevert_String[8]='2';//keys are not press on machine 

                                                                                        gucAmountCreditedPressKey=1;
                                                                                        gucBeginSessionTimerFlag=0;
                                                                                        gucMDBSessionStarted=1;
    //                                                                                    if (ucVendProcessRunning)
                                                                                        {
    //                                                                                      Start_Key_Pressing();
                                                                                        }

                                                                                        ACK_Stage_State=0;
                                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                        ucResPollStage=VMC_POLL_ACK;
                                                                                        gucMDB_Step=0;
                                                                                        break;
                                                            case ACK_RCV_VEND_APPROVE:
                                                                                        ACK_Stage_State=0;
                                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                        ucResPollStage=VMC_POLL_ACK;
                                                                                        break;
                                                            case ACK_RCV_END_SESSION:
                                                                                        ACK_Stage_State=0;
                                                                                        ucPresentMDB_Stage=0;
                                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                        ucResPollStage=VMC_POLL_ACK;
                                                                                        ucSession_END_ACkRcv=1;  // For  Vend Status send to APP
                                                                                        gucMDBSessionStarted=0;
                                                                                        break;
                                                            case ACK_RCV_SESSION_CANCEL:
                                                                                        ACK_Stage_State=0;
                                                                                        gucMDB_Step=0;
                                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                        ucResPollStage=VMC_POLL_ACK;
                                                                                        break;
                                                            case ACK_RCV_VEND_DENIED:
                                                                                        ACK_Stage_State=0;
                                                                                        MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                                        ucResPollStage=VMC_POLL_ACK;
                                                                                        gucMDB_Step=0;
                                                                                        break;
                                                            default                 :			
                                                                                        break;
                                                        }
                                                        break;
                                case VMC_RESET_M    :

                                                        break;
                                case VMC_RESET		:
                                                        if (guiLastMDB_RxData==VMC_RESET_M)
                                                        {
                                                            MDB_Response_Stage=RES_RESET_M;
//    														BLE_PutString("\r\n VMC_45");
                                                            ucRESET_RecvFLG=1;
                                                        }
                                                        break;
                                case VMC_SETUP     :
                                                        guiMDB_RecBuff_Index=0;
                                                        gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_SETUP;
                                                        gucMDB_Step=1;
                                                        break;
                                case VMC_POLL_M    :
                                                        break;
                                case VMC_POLL      :
                                                        if (ucRESET_RecvFLG)
                                                        {
//                                                            BLE_PutString("\r\n ucRESET_RecvFLG==1");
                                                            switch(MDB_NEXT_POLL_STATE)
                                                            {
                                                                case VMC_POLL_ACK  :   
                                                                                        MDB_Response_Stage=RES_POLL_ACK;
                                                                                        break;

                                                                case JUST_RESET             :
//                                                                                                BLE_PutString("\r\n VMC_123");
                                                                                                MDB_Response_Stage=RES_JUSTRESET;
                                                                                                break;
                                                                default						:
                                                                                                break;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            //
                                                            if (ucInitialMDBCommError)
                                                            {
                                                                MDB_PutChar(0x000);
                                                                MDB_PutChar(VMC_ACK);
//                                                                BLE_PutString("\r\nPOlll");
                                                            }
                                                        }
                                                        break;
                                case VMC_VEND      :
                                                        guiMDB_RecBuff_Index=0;
                                                        gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_VEND;
                                                        gucMDB_Step=4;
                                                        break;
                                case VMC_READER    :
                                                        guiMDB_RecBuff_Index=0;
                                                        gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_READER;
                                                        gucMDB_Step=3;
                                                        break;
                                case VMC_REVALUE    :  
                                                        guiMDB_RecBuff_Index=0;
                                                        gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_REVALUE;
                                                        gucMDB_Step=12;
                                                        break;
                                case VMC_EXPANSION :
                                                        guiMDB_RecBuff_Index=0;
                                                        gucMDB_RecBuff[guiMDB_RecBuff_Index++]=VMC_EXPANSION;
                                                        gucMDB_Step=2;
                                                        break;												
                                case VMC_NAK        :
    //                                                    BLE_PutString("\r\nMDB_NAK");                  
                                                        break;  
                                default			   :
                                                        break;
                            }
                            break;
                case 1:
                            //collecting the VMC SETP & VMC PRICE Data
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>=7)
                            {
                                MDB_Response_Stage=RES_CONFIGSETUP;
                            }
                            break;
                case 2:
                            //collecting the VMC EXPANSION data
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>31)
                            {
                                MDB_Response_Stage=RES_PERI_ID;
                            }
                            break;
                case 3:
                            //Collecting the VMC READER data
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>=3)
                            {
    //							MDB_Response_Stage=RES_READER;

                                unsigned char checksum = calc_checksum(gucMDB_RecBuff, 2);
                                checksum &= 0x00FF;

                                if (checksum != gucMDB_RecBuff[2])
                                {
                                    MDB_PutChar(VMC_NAK);
                                }
                                else
                                {
    //								BLE_PutString("\r\nVMC Reader Checksum Matched");
                                    switch(gucMDB_RecBuff[1])
                                    {
                                        case VMC_READER_DISABLE :
                                                                    Send_Acknowledgement();
                                                                    BLE_PutString("read disable\r\n");
                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                    ucResPollStage=VMC_POLL_ACK;
                                                                    gucMDB_ReaderEnable_Flag=0;
                                                                    gucMDB_ReaderCancel_Flag=0;
                                                                    gucMachineReadyToVend=0;
                                                                    LED_5=1; // LED OFF
                                                                    MDB_Response_Stage=0;
                                                                    gucMDB_Step=0;
                                                                    break;
                                        case VMC_READER_ENABLE  :

                                                                    MDB_Response_Stage=RES_READER;
                                                                    Send_Acknowledgement();
                                                                    BLE_PutString("read enable\r\n");
                                                                    MDB_NEXT_POLL_STATE=VMC_POLL_ACK;
                                                                    ucResPollStage=VMC_POLL_ACK;
                                                                    gucMDB_ReaderEnable_Flag=1;
                                                                    gucMDB_ReaderCancel_Flag=0;
                                                                    gucMachineReadyToVend=1;

                                                                    MDB_Response_Stage=0;
                                                                    gucMDB_Step=0;
                                                                    LED_5=0; // LED ON
                                                                    break;
                                        case VMC_READER_CANCEL  :
                                                                   MDB_Response_Stage=RES_READER;
                                                                    Vend_Cancelled();
    //																BLE_PutString("READER CANCEL\r\n");
                                                                    gucMDB_ReaderEnable_Flag=0;
                                                                    gucMDB_ReaderCancel_Flag=1;
                                                                    gucMachineReadyToVend=0;
                                                                    LED_5=1; // LED OFF
                                                                    break;
                                        default : break;
                                    }
                                }
                                MDB_Response_Stage=0;
                                gucMDB_Step=0;
                            }
                            break;
                case 4:
                            //Vend Request Data
                            switch(guiMDB_RxData)
                            {
                                case 0x00	:
                                                //vend request
                                                gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                                gucMDB_Step=5;
                                                break;
                                case 0x01	:
                                                //vend cancel
                                                gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                                gucMDB_Step=6;
                                                break;
                                case  0x02	:
                                                //vend success
                                                gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                                gucMDB_Step=7;
                                                break;
                                case 0x03	:
                                                //vend fail
                                                gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                                gucMDB_Step=8;
                                                break;
                                case 0x04	:
                                                //ssn complete
                                                gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                                gucMDB_Step=9;
                                                break;
                                case 0x05	:
                                                //Cash Vend
                                                gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                                gucMDB_Step=10;
                                                break;
                                default     :
                                                break;
                            }
                            break;
                case 5:
                            //Vend Request by VMC
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>=7)
                            {
                                MDB_Response_Stage=RES_VENDRQST;
                            }
                            break;
                case 6:
                            //vend cancel
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>=3)
                            {
                                MDB_Response_Stage=RES_VEND_CANCEL;
                            }
                            break;
                case 7:
                            //vend success
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>=5)
                            {
                                MDB_Response_Stage=RES_VNDSUCCESS;
                            }
                            break;
                case 8:
                            //vend fail
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>=3)
                            {
                                MDB_Response_Stage=RES_VEND_FAIL_SLAVE;
                            }
                            break;


                case 9:
                            //ssn complete
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if((guiMDB_RecBuff_Index>=2))
                            {
                                MDB_Response_Stage=RES_SESSIONCOMPLETE;
                            }
                            break;
                case 10:
                            //vend using coin
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>=7)
                            {
                                MDB_Response_Stage=RES_CASH_VEND_SLAVE;
                            }
                            break;
                case 11:
                            gucVMC_PER_ID[gucVMC_PER_ID_Index++]=guiMDB_RxData;
                            if (gucVMC_PER_ID_Index>25)
                            {
                                gucVMC_PER_ID_Index=0;
                                gucMDB_Step=0;
                            }
                            break;
                case 12: 	
                            switch(guiMDB_RxData)
                            {
                                case 0x00	:
                                                //Revalue request
                                                gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                                gucMDB_Step=13;
                                                break;
                                case 0x01	:
                                                //Revalue Limit
                                                gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                                                guiMDB_RecBuff_Index=0;
                                                gucMDB_Step=0;
                                                break;
                            }
                            break;

                case 13:  	
                            gucMDB_RecBuff[guiMDB_RecBuff_Index++]=guiMDB_RxData;
                            if(guiMDB_RecBuff_Index>=7)
                            {
                                MDB_Response_Stage=RES_REVALUE_RQST;
                            }
                            break;	
                default:
                            break;
            }

            guiLastMDB_RxData = guiMDB_RxData;
        }
        
        PIR1bits.RC1IF=0;
    }
    
    if(PIE3bits.RC2IE == 1 && PIR3bits.RC2IF == 1) // Blue-tooth UART ISR
    {
        if(1 == RCSTA2bits.OERR)
        {
            // EUSART2 error - restart

//            BLE_PutString("BORR\r\n");
            RCSTA2bits.CREN = 0;
            NOP();
            RCSTA2bits.CREN = 1;
        }
        else 
        {

            // buffer overruns are ignored
            bleRXData = RCREG2;
            {
                    //if(((bleRXData=='*') || (bleRXData=='$'))  && guiRxRF_Buffer_Index==0 && gucRxRF_Fr_rec_Flag==0)		// check for the start character & array count to received data
                    if((bleRXData=='$')  && (guiRxRF_Buffer_Index==0) && (gucRxRF_Fr_rec_Flag==0))		// check for the start character & array count to received data
                    {
                        gucRxRF_Buffer[guiRxRF_Buffer_Index]=bleRXData;		//save the data after start char
                        guiRxRF_Buffer_Index++;							// increment the index of array to save data
                        guc_St_RxRF_flag=1; 
                    }
                    else if((bleRXData=='*')  && (guiLastBLE_RxData=='0') && (guiRxRF_Buffer_Index==0) && (gucRxRF_Fr_rec_Flag==0))
                    {
                        gucRxRF_Buffer[guiRxRF_Buffer_Index]=bleRXData;		//save the data after start char
                        guiRxRF_Buffer_Index++;							// increment the index of array to save data
                        guc_St_RxRF_flag=1; 
                    }
                    else if(guc_St_RxRF_flag==1)                             //	check reception is on or not to save data
                    {
                        if((bleRXData=='*') || (bleRXData=='$'))                           // check the end of string character 
                        {
                            gucRxRF_Buffer[guiRxRF_Buffer_Index]=bleRXData;		//save the last character
                            guiRxRF_Buffer_Index++;							//increment array index
                            gucRxRF_Fr_rec_Flag=1;  
                        }
                        else if(guiRxRF_Buffer_Index>MaxChars)
                        {
                            guiRxRF_Buffer_Index=0;	
                            gucRxRF_Buffer[guiRxRF_Buffer_Index]='\0';		//save the character in array
                            gucRxRF_Fr_rec_Flag=0;
                            guc_St_RxRF_flag=0;
                        }
                        else
                        {
                            gucRxRF_Buffer[guiRxRF_Buffer_Index]=bleRXData;		//save the character in array
                            guiRxRF_Buffer_Index++;							// increase array index
                        }
                    }
                    
                    guiLastBLE_RxData=bleRXData;
            }
        }
        PIR3bits.RC2IF=0;
    }
    if(INTCONbits.TMR0IE == 1 && INTCONbits.TMR0IF == 1)
    {
        // TMR0H 120; 
        TMR0H = 0x78;

        // TMR0L 255; 
        TMR0L = 0xFF;
    
//// *******************************************	

        if (uc_VndReqstTimerFLG)
        {
            ui_VndReqstTimerFCount++;
            
            if (ui_VndReqstTimerFCount>450)
            {
//                BLE_PutString("Vending Timeout:\r\n");  
                ucVendingTimerExpiredFlg=1;
                uc_VndReqstTimerFLG=0;
                ui_VndReqstTimerFCount=0;
                if(gucMDBSessionStarted==1)
                {
//                    BLE_PutString("Vending Timeout SC:\r\n");  
                    gucCancel_Session=1;
                }
            }

        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        //loop to closed 
        if(gucStartRelayPressCounterFlag)
        {
            gucRelayPressCounter++;

             if((gucRelayPressCounter>=100))
             {
//                 BLE_PutString("Relay Timeout SC:\r\n");  
                 gucRelayPressCounter=0;
                 gucStartRelayPressCounterFlag=0;
                 gucCancel_Session=1;
             }
        }
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        //loop to restart the MDB Session if session not credited
        if(gucBeginSessionTimerFlag)
        {
            guiBeginSessionTimerCounter++;
            if(guiBeginSessionTimerCounter>=10)
            {
//                BLE_PutString("SSN Repeat\r\n");  
                guiBeginSessionTimerCounter=0;
                ucPresentMDB_Stage=1;
                ucResPollStage = BEGIN_SESSION;
            }
        }
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        
        // clear the TMR0 interrupt flag
        INTCONbits.TMR0IF = 0;
    }
    
}
