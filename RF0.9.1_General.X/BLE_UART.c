#include "basic_requirements.h"


/* initialize UART3 to transmit at 9600 Baud */
void initilize_BLE_uart(void) 
{	
	TRISD6 = 0;   					// TX Pin
	TRISD7 = 1;   					// RX Pin
    
    // disable interrupts before changing states
    PIE3bits.RC2IE = 0;
    
    // ABDOVF no_overflow; CKTXP async_noninverted_sync_fallingedge; BRG16 16bit_generator; WUE disabled; ABDEN disabled; DTRXP not_inverted; 
    BAUDCON2 = 0x08;

    // SPEN enabled; RX9 8-bit; CREN enabled; ADDEN disabled; SREN disabled; 
    RCSTA2 = 0x90;

    // TX9 8-bit; TX9D 0; SENDB sync_break_complete; TXEN enabled; SYNC asynchronous; BRGH hi_speed; CSRC slave_mode; 
    TXSTA2 = 0x24;

    // 
    SPBRG2 = 0x17;

    // 
    SPBRGH2 = 0x00;

    // enable receive interrupt
    PIE3bits.RC2IE = 1;
}

void BLE_PutChar(unsigned char data) 
{
    PIR3bits.TX2IF=0;
    while(0 == PIR3bits.TX2IF)
    {
    }
    NOP();
    NOP();//adding three extra NOP because to resolve cash string issue missing compartment(i.e. 1.44us delay)
    NOP();
    NOP();
    TXREG2 = data;    // Write the data byte to the USART.
}

void BLE_PutString(unsigned char *str)
{
	unsigned int i=0;
    while(str[i]!='\0')                 //check for NULL character to terminate loop
    {
        BLE_PutChar(str[i]);
        i++;
    }
}


void BLE_PutStringL(unsigned char *str,unsigned int len)
{
	unsigned int i=0;
    while(len>0)
    {       
        BLE_PutChar(str[i]);
        len--;
        i++; 
         __delay_ms(5);
    }
}

