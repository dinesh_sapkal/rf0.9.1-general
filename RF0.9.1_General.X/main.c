/*
 * File:   main.c
 * Author: User
 *
 * Created on June 28, 2021, 11:49 AM
 */


#include "basic_requirements.h"

unsigned int guiLED_Counter;
unsigned char gucSendMachineReadyFlag=0,gucMachineReadySendCount=0,wrg_flg=0;
unsigned char gucSendReadEnableFlag=0,gucReadEnableSendCount=0;
unsigned int guiMachineReadyCounter=0,guiReadEnableCounter=0;
 

void main(void) {
    
    
    pin_initilize();
    dBLE_RESET_PIN=0x01;
    __delay_ms(30);
    
    initilize_MDB_uart();
    initilize_BLE_uart();
    BLE_PutString("Hi Vendekin \r\n");
    TMR0_Initialize();
    i2c_init();
    
    #ifdef SEGA_NUMERIC_MACHINE
        BLE_PutString("Sega Numeric Machine\r\n");
    #endif

    #ifdef SEGA_ALPHANUMERIC_MACHINE
        BLE_PutString("Sega AlphaNumeric Machine\r\n");
    #endif

    #ifdef VENDIMAN_XY_MACHINE 
        BLE_PutString("Vendiman XY Machine\r\n");
    #endif

    #ifdef TCN_COMBO_WHITE_MACHINE
        BLE_PutString("TCN COMBO White Machine\r\n");
    #endif

    INTCONbits.PEIE = 1;        //enable global interrupt
    INTCONbits.GIE = 1; 
        
    ucRESET_RecvFLG=0;
    gucMDB_Step=0;	
    ACK_Stage_State = ACK_RCV_JUST_RESET;
    MDB_NEXT_POLL_STATE = JUST_RESET;

    //---------------------------------------------------------------------------
    // wait for the power on the machine properly
//    for(unsigned char h=0;h<40;h++)
//    for(unsigned char h=0;h<10;h++)
    #ifdef SEGA_NUMERIC_MACHINE
    for(unsigned char h=0;h<60;h++)
    {	
	 	__delay_ms(1000);
	 	LED_7=~LED_7;
		IsBLEConnected();	
	}
    #endif

    #ifdef SEGA_ALPHANUMERIC_MACHINE
    for(unsigned char h=0;h<60;h++)
    {	
	 	__delay_ms(1000);
	 	LED_7=~LED_7;
		IsBLEConnected();	
	}        
    #endif

    #ifdef VENDIMAN_XY_MACHINE 
//    for(unsigned int long h=0;h<1000000;h++)
//    {	
////	 	__delay_ms(100);
//	 	LED_7=~LED_7;
//		IsBLEConnected();	
//        MDB_Communication();
//	}
    for(unsigned char h=0;h<60;h++)
    {	
	 	__delay_ms(1000);
	 	LED_7=~LED_7;
		IsBLEConnected();	
	}
    #endif

    #ifdef TCN_COMBO_WHITE_MACHINE
    for(unsigned char h=0;h<60;h++)
    {	
	 	__delay_ms(1000);
	 	LED_7=~LED_7;
		IsBLEConnected();	
	}
    #endif
	
    //---------------------------------------------------------------------------
    
    //---------------------------------------------------------------------------
    //initialize the Power ON DEX
    gucPowerOnDEX_Flag=1;
//    gucPowerOnDEX_Flag=0;
    gucMachineReadyToVend=0;
    while(1)//Power on Dex Flag
    {
        IsBLEConnected();
        DEX_Communication();
        
        if(gucMachineReadyToVend)
            break;
    }
    //---------------------------------------------------------------------------
    

    if(gucMDB_ReaderEnable_Flag==0)
        gucMachineReadyToVend=0;
    
    
//    Send_Just_Reset();
    MDB_NEXT_POLL_STATE= JUST_RESET;   
    TMR0_StartTimer();    
//    ucRESET_RecvFLG=0;
//    ACK_Stage_State = ACK_RCV_JUST_RESET;
//    MDB_NEXT_POLL_STATE = JUST_RESET;
    gucLastVendStatus='0';
    

    
    
//    INTCONbits.PEIE = 1;        //enable global interrupt
//    INTCONbits.GIE = 1; 
    
    while (1)
    {
        guiLED_Counter++;
        if ((guiLED_Counter>=1000) && (gucPowerOnDEX_Flag==0) && (gucMDB_ReaderEnable_Flag==1))
        {
            TEST_LED = ~ TEST_LED;
            guiLED_Counter=0;
        }
        
        if((gucSendMachineReadyFlag==1) && (gucMachineReadySendCount<=5) && (gucVendStartFlag==0))
        {
            guiMachineReadyCounter++;
            if(guiMachineReadyCounter>20000)
            {
                guiMachineReadyCounter=0;
                BLE_PutString("Machine Ready\r\n");
                BLE_PutString("ACK_ENABLE\r\n");
                gucMachineReadySendCount++;  
            }
        }
        else if((gucSendReadEnableFlag==1) && (gucReadEnableSendCount<2) && (gucVendStartFlag==0))
        {
            guiReadEnableCounter++;
            if(guiReadEnableCounter>20000)
            {
                guiReadEnableCounter=0;
                BLE_PutString("read enable\r\n");
                gucReadEnableSendCount++;  
            }
        }
        IsBLEConnected();
        DEX_Communication();
        MDB_Communication();
		BLE_Communication();
	}
}
